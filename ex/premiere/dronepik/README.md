Ce projet est un dérivé d'un TP de NSI en juin 2020.

# Dronépik
**Site de vente de drones imitant les insectes**

## Langues
 - HTML
 - PHP
 - JavaScript
 - CSS

La majorité des pages est codée en HTML, stylisé par CSS. 
Les pages relatives à la commande et à la prise de contact sont en PHP.
Les interactions directes avec l'utilisateur sont en JavaScript.

## Privacy
Ce site utilise des cookies, mais leur utilisation est poussée au minimum. Ils sont utilisés pour conserver le numéro de client, le numéro de commande et la préférence d'affichage de la banière de cookies.

Seules les informations de commandes affichées sur la page de récapitulatif de commande et les informations entrées dans le formulaire de contact sont conservées sur le serveur.

Aucun script de tracage ou d'analyse d'audience n'est utilisé. 

## Disponibilité
Au 06/08/2020, ce site n'est pas disponible publiquement sur internet.

Vous pouvez en revanche le visionner localement sur votre ordinateur grâce à un logiciel tel que XAMPP ou WAMP.