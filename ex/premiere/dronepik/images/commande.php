<html>
<head>
    <meta charset='utf-8'>
    <title>Dronépik - Commande</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
    <link rel="stylesheet" href="commande.css">
    <link rel="icon" href="images/shopping-cart.svg">
</head>

<body onload="afficherCookies()">
        <div id="barreNav">
            <img src="images/logo.png" alt="">
            <nav>
                <a href="apropos.html">A Propos</a>
                <a class="active" href="commande.html">Commander</a>
                <a href="modeles.html">Modèles</a>
                <a href="index.html">Accueil</a>
            </nav>
        </div>


    <div id="page">
        <h1>Formulaire de commande</h1>

        <form action="enregistre_cmd.php" method="GET">
            <section>
                <label for="nom">Nom et Prénom :
                    <input type="text" name="nom" id="nom" placeholder="Nom" required>
                    <input type="text" name="nom" id="prenom" placeholder="Prénom" required>
                </label>
            </section>

            <section>
                <label for="modele">Modèle :</label>
                <select name="modele" required>
                    <option value=""></option>
                    <option value="150">Mouche</option>
                    <option value="210">Moustique</option>
                    <option value="180">Guêpe</option>
                    <option value="130">Bourdon</option>
                </select>
            </section>

            <section>
                <label>Résolution :</label>
                <div class="radio">
                    <input type="radio" name="resolution" id="p720" value="0">
                    <label for="p720">1280x720 pixels - HD</label>
                </div>
                <div class="radio">
                    <input type="radio" name="resolution" id="p1080" value="15">
                    <label for="p1080">1920x1080 pixels - Full HD</label>
                </div>
                <div class="radio">
                    <input type="radio" name="resolution" id="p4K" value="30">
                    <label for="p4K">4096x2160 pixels - 4K</label>
                </div>
            </section>

            <section>
                <label>Options :</label>
                <div class="check">
                    <input value="20" type="checkbox" name="options" id="bluetooth">
                    <label for="bluetooth">Connection bluetooth</label>
                </div>
                <div class="check">
                    <input value="20" type="checkbox" name="options" id="detecteurMouvement">
                    <label for="detecteurMouvement">Detecteur de mouvement</label>
                </div>
                <div class="check">
                    <input value="20" type="checkbox" name="options" id="bourdonnement">
                    <label for="bourdonnement">Bourdonnement réaliste</label>
                </div>
            </section>

            <fieldset>
                <legend>Total</legend>
                <p>0 €</p>
            </fieldset>


            <input id="enregistrer" type="submit" value="Enregistrer la commande" onclick="contenuCommande()">
        </form>

        <form action="suppressionCommandes.php">
            <input type="password" name="mdp" id="suppression" >
        </form>




        <footer>
            © Logo créé avec <a href="https://logomakr.com/">LogoMakr</a>, Icône de <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>.
        </footer>
    </div>



    <div id="boite">
        <h2>Ce site utilise des cookies</h2>
        <p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour enregistrer votre panier et réaliser des statistiques de visites.</p>
        <button><a href="cookies.html">En savoir plus</a></button>
        <button onclick="fermer()">OK</button>
    </div>
</body>



<script>
    const boiteCookies = document.getElementById("boite");
    var prixTotal = 0;
    var prixAAjouter;
    var prixResolution;
    var prixOptions = [];
    const formulaire = document.getElementsByTagName('form')[0];
    var modeleChoisi;
    const zoneTotal = document.getElementsByTagName('p')[0];
    const body = document.body;


    //-----Mise à jour du total à chaque clic de la souris-----
    formulaire.addEventListener('change', prix);



    //------------------Affichage du prix----------------------
    function prix() {
        prixTotal = 0;
        
        for(var i = 0; i < formulaire.elements["resolution"].length; i++) {
            if(formulaire.elements["resolution"][i].checked) {
                prixResolution = parseInt(formulaire.elements["resolution"][i].value);
            }
        }

        for(var i = 0; document.getElementsByName('options')[i]; ++i){
            if(document.getElementsByName('options')[i].checked){
                prixOptions.push(parseInt(document.getElementsByName('options')[i].value));
            }
        }

        modeleChoisi = formulaire.elements["modele"].selectedIndex;

        prixAAjouter = [parseInt(formulaire.elements["modele"].options[modeleChoisi].value), prixResolution];

        for (var i in prixOptions) {
            prixAAjouter.push(prixOptions[i])
        }

        for (var i in prixAAjouter) {
            prixTotal = (prixTotal + prixAAjouter[i]);
        }


        if (isNaN(prixTotal)) {
            prixResolution = 0;
            prixOptions= [];
            return prixTotal;
        } else {
            zoneTotal.innerHTML = prixTotal + " €";
        }


        prixResolution = 0;
        prixOptions= [];

        return prixTotal;
    }


    //---------------Afficher la banière cookies---------------
    function afficherCookies() {
        if (document.cookie == "baniereCookies=cacher") {
            boiteCookies.style.display = "none";
        } else {
            return;
        }

        prix();
    }

    //----------------Fermer la banière cookies----------------
    function fermer() {
        boiteCookies.style.display = "none";
        document.cookie = "baniereCookies=cacher;max-age=1209600";
    }
</script>

</html>