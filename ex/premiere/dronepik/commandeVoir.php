<?php 
    ini_set("log_errors", 1);
    ini_set("error_log", "erreurs_php.log");

    #Récupération de l'adresse IP pour le numéro client
    function recupAdresseIp() {
        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
          $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
          $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
          $ip = $_SERVER['REMOTE_ADDR'];
        }
        return hash('md5', $ip);
    }



    #Enregistrement des données entrées dans le formulaire
    function enregistrerDonnees() {
        #Récupération des données du fomulaire
        $nom = $_POST['nom'];
        $prenom = $_POST['prenom'];
        $adresse = $_POST['adresse'];
        $cP = $_POST['codePostal'];
        $ville = $_POST['ville'];
        $telephone = $_POST['telephone'];
        $email = $_POST['email'];

        #Vérification du numéro client
        $numClient = $_COOKIE['numeroClient'];

        if (!isset($numClient)) {
            $numClient = recupAdresseIp();
        }


        #Ajout des données au fichier commande du client
        if ($nom != "" and $prenom != "" and $adresse != "" and $cP != "" and $ville != "" and $telephone != "" and $email != "") {
            $fichierCommande = fopen("commandes/$numClient.csv", "a");
            if ($fichierCommande) {
                $contenuCommande = "$nom;$prenom;$adresse;$cP;$ville;$telephone;$email;\n";
                fputs($fichierCommande, $contenuCommande);
    
                fclose($fichierCommande);
            }
        } else {
            header('Location: commande.php'); #Retour à la page de commande initiale si arguments vides
        }
    }




    #Affichage de la commande enregistrée dans le fichier client
    function affichageCommande($numClient) {
        $commandeBrut = "";

        $fichierCommande = fopen("commandes/$numClient.csv", "r");

        if ($fichierCommande) {
            while(! feof($fichierCommande)) {
                $ligne = fgets($fichierCommande);
                $commandeBrut = $commandeBrut . $ligne;
            }

            $commande = explode(";", $commandeBrut);

            fclose($fichierCommande);
            return $commande;
        } else {
            return "Une erreur est survenue. Merci de réessayer plus tard. Vous pouvez aussi nous la signaler via le formulaire de contact dans la rubrique Contact.";
        }
    }



    #Verification de la page de provenance pour affichage ou non de l'écriteau 'Commande déjà passée'
    function commandePassee() {
        if ($_SERVER['HTTP_REFERER'] == "http://localhost/dronepik/commande2.php") {
            return "style='display:none'";
        } else {
            return;
        }
    }


    #Vérification de l'existance d'un fichier client pour savoir s'il a déjà passé commande
    $numClient = $_COOKIE['numeroClient'];

    if (!isset($numClient)) {
        $numClient = recupAdresseIp();
    }

    if (!file_exists("commandes/$numClient.csv")) {
        header("Location: commande.php");
    }
    


    #Vérification de l'origine de la requête avant d'enregistrer les données du formulaire
    if (!empty($_SERVER['HTTP_REFERER'])) {
        if ($_SERVER['HTTP_REFERER'] == "http://localhost/dronepik/commande2.php") {
            enregistrerDonnees();
        }
    }
?>


<html>
<head>
    <meta charset='utf-8'>
    <title>Dronépik - Commande</title>
    <link rel='stylesheet' type='text/css' media='screen' href='style.css'>
    <link rel="stylesheet" type='text/css' media='screen' href="commande.css">
    <link rel="icon" href="images/shopping-cart.svg">
</head>

<body onload="afficherCookies()">
        <div id="barreNav">
            <img src="images/logo.png" alt="">
            <nav>
                <a href="apropos.html">A Propos</a>
                <a href="contact.html">Contact</a>
                <a class="active" href="commande.php">Commander</a>
                <a href="modeles.html">Modèles</a>
                <a href="index.html">Accueil</a>
            </nav>
        </div>


    <div id="page">
        <h1>Commande réussie</h1>

        <div id="progressBar">
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
            <div class="ligne"></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
            <div class="ligne"></div>
            <svg xmlns="http://www.w3.org/2000/svg" width="34" height="34" viewBox="0 0 64 64" enable-background="new 0 0 64 64"><path fill="#83bf4f" d="M58.4 0 23.6 38.3 13.5 30.6 7.9 30.6 23.6 56.1 64 0z"/><path d="m53.9 56.1c0 .6-.5 1.1-1.1 1.1h-44.9c-.6 0-1.1-.5-1.1-1.1v-44.9c0-.6.5-1.1 1.1-1.1h30.7l6.1-6.7h-42.5c-1.2 0-2.2 1-2.2 2.2v56.1c0 1.3 1 2.3 2.2 2.3h56.1c1.2 0 2.2-1 2.2-2.2v-43.7l-6.7 9.4c.1 0 .1 28.6.1 28.6" fill="#3e4347"/></svg>
        </div>


        <p>Nous accusons réception de votre commande n°<b><?php $commande = affichageCommande($numClient); echo $commande[0]; ?></b> chez Dronépik.</p>
        <p>Contenu de votre commande : <br>
            <span id="reception">
                
                <?php
                    $options = "";
                    
                    if ($commande[3] == "oui") {
                        $options = "Bluetooth";
                    }

                    if ($commande[4] == "oui" and $options == "") {
                        $options = "Detecteur de mouvements";
                    } else if ($commande[4] == "oui" and $options != "") {
                        $options = $options . ", Detecteur de mouvements";
                    }

                    if ($commande[5] == "oui" and $options == "") {
                        $options = "Bourdonnement réaliste";
                    } else if ($commande[5] == "oui" and $options != "") {
                        $options = $options . ", Bourdonnement réaliste";
                    }

                    if ($options == "") {
                        $options = "Aucune option";
                    }
                    
                    echo "<br>
                    <b>Nom et Prénom :</b> $commande[7] $commande[8]<br>
                    <b>Modèle :</b> $commande[1]<br>
                    <b>Résolution :</b> $commande[2]<br>
                    <b>Options :</b> $options<br>
                    <b>Prix :</b> $commande[6]€<br><br>
                    <b>Adresse :</b> $commande[9], $commande[10] $commande[11]<br>
                    <b>Téléphone :</b> $commande[12]<br>
                    <b>Adresse mail :</b> $commande[13]";
                ?>
               
            </span>
            
            <br>
            Votre colis devrait arriver chez vous sous 2 semaines.
        </p>
        


        <footer>
            © Logo créé avec <a href="https://logomakr.com/">LogoMakr</a>, Icône de <a href="https://www.flaticon.com/authors/kiranshastry" title="Kiranshastry">Kiranshastry</a>.
        </footer>
    </div>


    <div id="boiteBlanche" <?php echo commandePassee(); ?>>
        <h2>Vous avez déjà passé une commande récemment</h2>
        <p>Par mesure anti-spam, le délai minimum entre deux commandes est d'une semaine</p>
        <a onclick="setTimeout(fermerDejaPasse(), 1000)">Ok, j'attendrai</a>
    </div>



    <div id="boite">
        <h2>Ce site utilise des cookies</h2>
        <p>En poursuivant votre navigation sur ce site, vous acceptez l’utilisation de Cookies pour enregistrer votre panier et réaliser des statistiques de visites.</p>
        <button><a href="cookies.html">En savoir plus</a></button>
        <button onclick="fermerBaniere()">OK</button>
    </div>
</body>



<script>
    const dejaPasse = document.getElementById('boiteBlanche');
    const boiteCookies = document.getElementById("boite");
    const cookieBaniere = document.cookie.split('; ').find(row => row.startsWith('baniereCookies'));


    //--------Fermer le signalement Commande déjà passée---------
    function fermerDejaPasse() {
        dejaPasse.className ='disparition';
    }

    //----------------Afficher la banière cookies----------------
    function afficherCookies() {
        if (cookieBaniere == "baniereCookies=cacher") {
            return;
        } else {
            boiteCookies.style.display = "block";
        }
    }

    //-----------------Fermer la banière cookies-----------------
    function fermerBaniere() {
        boiteCookies.style.display = "none";
        document.cookie = "baniereCookies=cacher;max-age=604800";
    }
</script>

</html>