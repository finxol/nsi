# Spécialité NSI

Bonjour ! 
Ceci est le dépot de fichiers pour le site de **NSI** fait en collaboration avec les élèves suivant cette spécialité au lycée **Joliot-Curie** à Rennes.


# Objectif
Le but de cette initiative est de créer un site dédié à la NSI et plus précisément de présentation de cette nouvelle matière au bac. 


# Open Source

Avec ce dépot et sa licence GPLv3, tout le monde a la possibilité de reprendre ces fichiers et de les remanier comme il l'entend. Cela ouvre donc la possibilité aux élèves de NSI, ou toute autre personne souhaitant apprendre le langage Web, de s'entrainer sans forcément partir de zéro.

## Liberté

Vous avez donc l'autorisation de prendre les fichiers de ce site, de les copier, de les redistribuer comme bon vous semble

## Limites et modifications

Cependant, pour éviter la perte d'un site correct, la possiblité de modifier ces fichiers directement dans ce dépot n'est pas accordée. 
>De plus, vous avez l'obligation de proposer ouvertement le code source de votre projet si vous comptez emprunter les fichiers de celui-ci

## Sommaire des autorisations

Au sein de ce projet, vos droits et interdits sont les suivants :

- Droit de copie
- Droit de partage 
- Droit de lecture
- Interdiction de modification
- Obligation d'ouverture du code source si redistribution


# Publication

Pour l'instant, ce projet n'est pas encore publié comme site web, mais cela ne saurait tarder (nous l'esperons !).

Pour l'heure, le code source est disponible [ici](https://gitlab.com/user038418/nsi).

# Amusez vous bien !
Après avoir pris connaissance et compris les termes de la licence, essayez des choses et amusez vous !

*© Les élèves de NSI du lycée Joliot-Curie à Rennes*